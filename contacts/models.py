# Django
from django.db import models

# Contrib, 3rd Party
from django_countries.fields import CountryField


class Contact(models.Model):
    """Abstract Main Contact Model."""

    address_line = models.CharField('address', max_length=250)
    city = models.CharField('city', max_length=60)
    state = models.CharField('state', max_length=30)
    postcode = models.CharField('post code', max_length=8, blank=True)
    country = CountryField('country')

    phone_number = models.CharField('phone number', max_length=12, blank=True)
    fax = models.CharField('fax', max_length=12, blank=True)
    mobile_number = models.CharField('mobile number', max_length=12, blank=True)

    web = models.URLField('web url', blank=True)
    email = models.EmailField('e-mail', max_length=254, blank=True)

    # owner = models.ForeignKey(Person, on_delete=models.CASCADE)
    # company_owner = models.ForeignKey(Company, on_delete=models.CASCADE)

    class Meta:
        """Metaclass for Contact class."""

        verbose_name = 'contact'
        verbose_name_plural = 'contacts'
        abstract = True


class Person(Contact):
    """Main Person Model."""

    SALUTATION_CHOICES = (
        ('MR', 'Mister'),
        ('MS', 'Miss'),
        ('DR', 'Doctor'),
        ('PhD', 'Professor'),
        ('LW', 'Lawyer')
    )

    salutation = models.CharField(max_length=3, choices=SALUTATION_CHOICES, blank=True)
    first_name = models.CharField('first name', max_length=24)
    middle_name = models.CharField('middle name', max_length=24, blank=True)
    last_name = models.CharField('last name', max_length=24)

    rec_date = models.DateTimeField('date recorded')

    class Meta:
        """Meta class for Person class."""

        verbose_name = 'person'
        verbose_name_plural = 'persons'

    def __str__(self):
        """Return Person's full name."""
        return '%s %s %s' % (self.first_name, self.middle_name, self.last_name)


class Company(Contact):
    """Main Company Model."""

    company_name = models.CharField('company name', max_length=250)

    # company_rec_date = models.DateTimeField('date recored', auto_now_add=True)

    class Meta:
        """Meta class for Company class."""

        verbose_name = 'company'
        verbose_name_plural = 'companies'

    def __str__(self):
        """Return company name."""
        return self.company_name
