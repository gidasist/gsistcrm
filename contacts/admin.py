# Django
from django.contrib import admin

# local Django
from .models import Company, Person
# Register your models here.

admin.site.register(Company)
admin.site.register(Person)
