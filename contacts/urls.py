from django.urls import path

from . import views


app_name = 'contacts'
urlpatterns = [
    path('', views.index, name='index'),
    path('person/', views.PersonList.as_view(), name='person'),
    path('person/<int:pk>', views.PersonDetail.as_view(), name='persondetail'),
    path('company/', views.CompanyList.as_view(), name='company'),
    path('company/<int:pk>', views.CompanyDetail.as_view(), name='companydetail')
]
