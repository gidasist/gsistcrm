# Django
from django.shortcuts import render
from django.views import generic

# Local
from .models import Person, Company


# Create your views here.
def index(request):
    """Main index view."""
    return render(request, 'contacts/index.html')


class PersonList(generic.ListView):
    """ListView for Contacts."""

    model = Person
    # template_name = 'contacts/contacts.html'
    context_object_name = 'contact_list'

    def get_context_data(self, **kwargs):
        """Get model fields for table view."""
        context = super().get_context_data(**kwargs)
        context['field_list'] = Person._meta.get_fields()
        return context


class PersonDetail(generic.DetailView):
    """DetailView for Persons."""

    model = Person


class CompanyList(generic.ListView):
    """ListView for Companies."""

    model = Company
    context_object_name = 'company_list'

    def get_context_data(self, **kwargs):
        """Get model fields for table view."""
        context = super().get_context_data(**kwargs)
        context['field_list'] = Company._meta.get_fields()
        return context


class CompanyDetail(generic.DetailView):
    """CompanyDetail for Company."""

    model = Company
